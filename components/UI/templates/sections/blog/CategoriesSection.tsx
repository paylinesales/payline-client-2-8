import React, { ReactElement } from "react";
import WhiteButton from "@/components/UI/atoms/WhiteButton";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const CategoriesSection = ({ categoriesData }): ReactElement => {
  const { edges } = categoriesData;

  const onClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  return (
    <div className="flex justify-center gap-3 my-12 mx-4">
      {edges.map((category) => {
        // need to use slider.js -> look at home screen categories
        return (
          <WhiteButton
            onClick={onClick}
            key={category.node.slug}
            disabled={false}
            className="rounded-full">
            {category.node.name}
          </WhiteButton>
        );
      })}
    </div>
  );
};

export default CategoriesSection;

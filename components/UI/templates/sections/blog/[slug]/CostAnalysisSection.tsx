import Image from "next/image";
import { ReactElement } from "react";
import ToggleWhiteBox from "../../../../atoms/ToggleWhiteBox";

const CostAnalysisSection = (): ReactElement => {
  return (
    <div className="p-6">
      <div className="flex justify-center">
        <div className="w-full md:w-1/2">
          <p className="text-lg font-hkgrotesk leading-tight font-bold text-payline-black flex flex-wrap justify-center">
            <span className="py-1">Receive a</span>
            <span className="flex mx-2 text-payline-white px-2 items-center bg-payline-peach">
              free
            </span>
            <span className="py-1">cost analysis</span>
          </p>
          <div className="text-center">
            <div className="block mt-3">
              <Image
                src="/images/svg/in-touch-top-arrow.svg"
                width={190}
                height={88}
              />
            </div>
            <div className="block">
              <Image src="/images/svg/sales-team.svg" width={190} height={88} />
              <div className="font-bold text-2xl text-payline-dark">
                Sales Team
              </div>
              <div className="text-center">Online now</div>
            </div>
            <div className="block">
              <Image
                src="/images/svg/in-touch-bottom-arrow.svg"
                width={190}
                height={88}
              />
              <div className="flex flex-wrap justify-center items-center -ml-5">
                <button
                  type="button"
                  className="font-bold uppercase my-5 items-center gap-3 px-8 rounded-full bg-payline-black text-white text-payline-dark p-3">
                  Request a Callback
                </button>
                <div className="w-full">Call now</div>
                <div className="font-bold w-full">(800)-887-3877</div>
              </div>
            </div>
          </div>
        </div>

        <div className="hidden md:block md:w-1/2 px-16">
          <ToggleWhiteBox title="First Two Months Free" key={0}>
            We put our money where our mouth is, check us out with no risk
          </ToggleWhiteBox>
          <ToggleWhiteBox
            title="Dedicated Account Manager and Support Team"
            key={1}>
            We put our money where our mouth is, check us out with no risk
          </ToggleWhiteBox>
          <ToggleWhiteBox title="Statement Analysis" key={2}>
            We put our money where our mouth is, check us out with no risk
          </ToggleWhiteBox>
          <ToggleWhiteBox
            title="Statement Analysis, Onboarding session, First Two Months Free "
            key={3}>
            We put our money where our mouth is, check us out with no risk
          </ToggleWhiteBox>
        </div>
      </div>
    </div>
  );
};

export default CostAnalysisSection;

// import TextInput from "@/components/UI/atoms/TextInput";
// import { Formik, Form, Field } from "formik";
// import * as Yup from "yup";
import { ReactElement } from "react";
import HubspotForm from "react-hubspot-form";
// const validationSchema = Yup.object({
//   firstName: Yup.string().required("First Name is Required"),
//   lastName: Yup.string().required("Last Name is Required"),
//   email: Yup.string().email("Invalid email address"),
//   websiteUrl: Yup.string().url("Invalid URL"),
//   phone: Yup.string(),
// });

// const initialValues = {
//   firstName: "",
//   lastName: "",
// };

const SignUpForm = (): ReactElement => {
  // const onSubmit = () => {
  //   console.log("clicked");
  // };
  // const { formId, portalId, region } = data;

  return (
    <div className="px-8">
      <HubspotForm
        region="na1"
        portalId="9154465"
        formId="3327d338-0840-4943-97af-5a5891d40e3d"
      />
      {/* <Formik
        onSubmit={onSubmit}
        initialValues={initialValues}
        validationSchema={validationSchema}>
        {() => (
          <Form>
            <div className="flex flex-col">
              <div className="w-full">
                <Field name="firstName">
                  {({ field, meta }) => (
                    <TextInput
                      htmlFor="firstName"
                      inputId="firstName"
                      inputName="firstName"
                      inputType="text"
                      autoComplete="firstName"
                      required
                      labelText="First Name"
                      field={field}
                      meta={meta}
                    />
                  )}
                </Field>
              </div>

              <div className="w-full">
                <Field name="lastName">
                  {({ field, meta }) => (
                    <TextInput
                      htmlFor="lastName"
                      inputId="lastName"
                      inputName="lastName"
                      inputType="text"
                      autoComplete="lastName"
                      required
                      labelText="Last Name"
                      field={field}
                      meta={meta}
                    />
                  )}
                </Field>
              </div>

              <div className="w-full">
                <Field name="businessWebsite">
                  {({ field, meta }) => (
                    <TextInput
                      htmlFor="businessWebsite"
                      inputId="businessWebsite"
                      inputName="businessWebsite"
                      inputType="text"
                      autoComplete="businessWebsite"
                      required
                      labelText="Business Website"
                      field={field}
                      meta={meta}
                    />
                  )}
                </Field>
              </div>

              <div className="w-full">
                <Field name="email">
                  {({ field, meta }) => (
                    <TextInput
                      htmlFor="email"
                      inputId="email"
                      inputName="email"
                      inputType="text"
                      autoComplete="email"
                      required
                      labelText="Email Address"
                      field={field}
                      meta={meta}
                    />
                  )}
                </Field>
              </div>

              <div className="w-full">
                <Field name="phone">
                  {({ field, meta }) => (
                    <TextInput
                      htmlFor="phone"
                      inputId="phone"
                      inputName="phone"
                      inputType="text"
                      autoComplete="phone"
                      required
                      labelText="Phone"
                      field={field}
                      meta={meta}
                    />
                  )}
                </Field>
              </div>
              <button type="submit" className="align-self-end">
                Sign Up
              </button>
            </div>
          </Form>
        )}
      </Formik> */}
    </div>
  );
};

export default SignUpForm;
